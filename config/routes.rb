Rails.application.routes.draw do

  resources :comments

  root 'pages#index'
end
